# `addToCart(items)`

## Dependents

RamdaJS

ED3 EventEmitter

| Param                       | Type             | Description                             |
| :-------------------------- | :--------------- | :-------------------------------------- |
| `items`                     | `array`          | An Array of Objects with item(s) to add |
| `expectedOrderFormSections` | `array`          | Fields to retrieve                      |
| `salesChannel`              | `integer/string` | Sales Channel id                        |

## Events

Use ED3 EventEmitter

| Event                       | Type      | Return             | Description |
| :-------------------------- | :-------- | :----------------- | :---------- |
| `PRODUCT_ADDED_TO_CART`     | `SUCCESS` | `orderForm, items` |             |
| `PRODUCT_NOT_ADDED_TO_CART` | `ERROR`   | `err, items`       |             |

## Example

```js
var items = [
  {
    id: 1,
    quantity: 1,
    seller: '1',
  },
  {
    id: 2,
    quantity: 2,
    seller: '1',
  },
];

addToCart(items);

EventEmitter.listen('PRODUCT_ADDED_TO_CART', (orderForm, items) => {
  console.log(orderForm, items);
});

EventEmitter.listen('PRODUCT_NOT_ADDED_TO_CART', (err, items) => {
  console.log(err, items);
});
```
