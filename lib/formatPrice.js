"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var formatPrice = function formatPrice(value) {
  var thousands = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : '.';
  var decimals = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : ',';
  var length = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : 2;
  var currency = arguments.length > 4 && arguments[4] !== undefined ? arguments[4] : 'R$ ';
  var regex = "\\d(?=(\\d{".concat(3, "})+", length > 0 ? '\\D' : '$', ")");
  var val = (value / 100).toFixed(Math.max(0, ~~length));
  return currency + val.replace('.', decimals).replace(new RegExp(regex, 'g'), "$&".concat(thousands));
};

var _default = formatPrice;
exports.default = _default;