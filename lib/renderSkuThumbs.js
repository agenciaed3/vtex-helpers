"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var renderSkuThumbs = function renderSkuThumbs(skus) {
  return skus.map(function (sku) {
    return "<li class=\"skus__item\"><span class=\"skus__label skuespec_Cor_opcao_01donademim\"></span></li>";
  }).join('');
};

var _default = renderSkuThumbs;
exports.default = _default;