"use strict";

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var R = _interopRequireWildcard(require("ramda"));

var getTotalItemsOrderForm = function getTotalItemsOrderForm(orderForm) {
  return R.find(R.propEq('id', 'Items'))(orderForm.totalizers);
};

var _default = getTotalItemsOrderForm;
exports.default = _default;