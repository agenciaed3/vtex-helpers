"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var R = _interopRequireWildcard(require("ramda"));

var _eventEmitter = _interopRequireDefault(require("@ed3digital/event-emitter"));

var addToCart = function addToCart(items) {
  var expectedOrderFormSections = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;
  var salesChannel = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : 1;

  if (!Array.isArray(items)) {
    throw new TypeError("Items precisa ser um Array de Objeto(s) com items para adicionar, exemplo: var items = [{id: 123, quantity: 1, seller: '1'}, {id: 321, quantity: 2, seller: '1'}]");
  }

  if (R.isEmpty(items)) {
    throw new Error("Items n\xE3o pode ser um array vazio.");
  }

  vtexjs.checkout.addToCart(items, expectedOrderFormSections, salesChannel).done(function (orderForm) {
    _eventEmitter.default.emit('PRODUCT_ADDED_TO_CART', [orderForm, items]);
  }).fail(function (err) {
    _eventEmitter.default.emit('PRODUCT_NOT_ADDED_TO_CART', [err, items]);
  });
};

var _default = addToCart;
exports.default = _default;