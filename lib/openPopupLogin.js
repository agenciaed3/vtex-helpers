"use strict";

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var R = _interopRequireWildcard(require("ramda"));

var openPopupLogin = function openPopupLogin(noReload, _url) {
  noReload = R.is(Boolean, noReload) ? noReload : false;
  _url = R.is(String, _url) ? _url : '/';
  _url = noReload ? window.location.href : _url;
  vtexid.start({
    returnUrl: _url
  });
};

var _default = openPopupLogin;
exports.default = _default;