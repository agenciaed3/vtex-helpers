"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var resizeImage = function resizeImage(src, width) {
  var height = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : 'auto';
  console.log(src);
  var newSrc = src.replace(/(?:ids\/[0-9]+)-([0-9]+)-([0-9]+)\//, function (match, matchedWidth, matchedHeight) {
    return match.replace("-".concat(matchedWidth, "-").concat(matchedHeight), "-".concat(width, "-").concat(height));
  });
  return newSrc.replace(/(ids\/[0-9]+)\//, "$1-".concat(width, "-").concat(height, "/"));
};

var _default = resizeImage;
exports.default = _default;