"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var getProductImages = function getProductImages(sku) {
  return {
    images: sku.images
  };
};

var _default = getProductImages;
exports.default = _default;