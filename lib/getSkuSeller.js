"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var getSkuSeller = function getSkuSeller(sku, sellerId) {
  var sellers = sku.sellers;
  var seller = sellerId || true;
  var sellerKey = sellerId ? 'sellerId' : 'sellerDefault';
  return sellers.find(function (item) {
    return item[sellerKey] === seller;
  });
};

var _default = getSkuSeller;
exports.default = _default;