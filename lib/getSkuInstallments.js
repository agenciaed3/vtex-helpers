"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _getSkuSeller2 = _interopRequireDefault(require("./getSkuSeller"));

var getSkuInstallments = function getSkuInstallments(sku, sellerId) {
  var _getSkuSeller = (0, _getSkuSeller2.default)(sku, sellerId),
      commertialOffer = _getSkuSeller.commertialOffer;

  var Installments = commertialOffer.Installments; // Get by min price value

  return Installments.reduce(function (prev, current) {
    return prev.Value < current.Value ? prev : current;
  }, {});
};

var _default = getSkuInstallments;
exports.default = _default;