"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

/**
 * @description
 * Formats price from Vtex API `/api/catalog_system/pub/products/search/`
 * to a correct `formatPrice` method
 *
 * @param  {number} val Value to convert
 * @returns {number}
 */
var fixProductSearchPrice = function fixProductSearchPrice(val) {
  return val && +val.toFixed(2).split('.').join('');
};

var _default = fixProductSearchPrice;
exports.default = _default;