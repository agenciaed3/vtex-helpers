"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _getSkuSeller = _interopRequireDefault(require("./getSkuSeller"));

var _getSkuInstallments = _interopRequireDefault(require("./getSkuInstallments"));

var _fixProductSearchPrice = _interopRequireDefault(require("./fixProductSearchPrice"));

var getSkuPrice = function getSkuPrice(sku, sellerId) {
  var seller = (0, _getSkuSeller.default)(sku, sellerId);
  var co = seller.commertialOffer;
  var installments = (0, _getSkuInstallments.default)(sku);
  var quantity = co.AvailableQuantity;
  return {
    available: Boolean(quantity),
    availableQuantity: quantity,
    seller: seller.sellerName,
    sellerId: seller.sellerId,
    hasListPrice: co.Price !== co.ListPrice,
    bestPrice: (0, _fixProductSearchPrice.default)(co.Price) || quantity,
    listPrice: (0, _fixProductSearchPrice.default)(co.ListPrice),
    installments: installments.NumberOfInstallments,
    installmentsValue: (0, _fixProductSearchPrice.default)(installments.Value),
    installmentsInterestRate: installments.InterestRate
  };
};

var _default = getSkuPrice;
exports.default = _default;