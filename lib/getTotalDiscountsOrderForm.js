"use strict";

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var R = _interopRequireWildcard(require("ramda"));

var getTotalDiscountsOrderForm = function getTotalDiscountsOrderForm(orderForm) {
  return R.find(R.propEq('id', 'Discounts'))(orderForm.totalizers);
};

var _default = getTotalDiscountsOrderForm;
exports.default = _default;