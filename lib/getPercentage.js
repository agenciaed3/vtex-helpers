"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var getPercentage = function getPercentage(oldPrice, newPrice) {
  var length = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : 0;

  if (oldPrice < newPrice || oldPrice < 1 || newPrice < 1) {
    return false;
  }

  var percent = newPrice / oldPrice * 100 - 100;
  var percentage = Math.abs(+percent.toFixed(length));
  return percentage;
};

var _default = getPercentage;
exports.default = _default;