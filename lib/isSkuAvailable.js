"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var isSkuAvailable = function isSkuAvailable(sku, sellerId) {
  var seller = getSkuSeller(sku, sellerId);
  return seller && seller.commertialOffer && seller.commertialOffer.AvailableQuantity > 0;
};

var _default = isSkuAvailable;
exports.default = _default;