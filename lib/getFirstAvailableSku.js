"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var getFirstAvailableSku = function getFirstAvailableSku(items) {
  return items.find(function (_ref) {
    var sellers = _ref.sellers;
    return sellers.find(function (_ref2) {
      var commertialOffer = _ref2.commertialOffer;
      return commertialOffer.AvailableQuantity > 0;
    });
  });
};

var _default = getFirstAvailableSku;
exports.default = _default;