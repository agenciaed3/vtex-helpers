"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var getProductIdInPage = function getProductIdInPage() {
  return document.getElementById('___rc-p-id').value;
};

var _default = getProductIdInPage;
exports.default = _default;