"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _axios = _interopRequireDefault(require("axios"));

var checkLogin = function checkLogin() {
  var endpoint = '/no-cache/profileSystem/getProfile';
  return new Promise(function (resolve, reject) {
    _axios.default.get(endpoint).then(function (res) {
      return resolve(res.data);
    }).catch(function (res) {
      return reject(res);
    });
  });
};

var _default = checkLogin;
exports.default = _default;