"use strict";

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var R = _interopRequireWildcard(require("ramda"));

var getTotalShippingOrderForm = function getTotalShippingOrderForm(orderForm) {
  return R.find(R.propEq('id', 'Shipping'))(orderForm.totalizers);
};

var _default = getTotalShippingOrderForm;
exports.default = _default;