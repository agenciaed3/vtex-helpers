const formatPrice = (value, thousands = '.', decimals = ',', length = 2, currency = 'R$ ') => {
  const regex = `\\d(?=(\\d{${3}})+${length > 0 ? '\\D' : '$'})`;
  const val = (value / 100).toFixed(Math.max(0, ~~length)) 

  return currency + val.replace('.', decimals).replace(new RegExp(regex, 'g'), `$&${thousands}`);
}

export default formatPrice;