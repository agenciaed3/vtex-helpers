import * as R from 'ramda';
const getTotalShippingOrderForm = (orderForm) => {
  return R.find(R.propEq('id', 'Shipping'))(orderForm.totalizers);
};

export default getTotalShippingOrderForm;
