const getFirstAvailableSku = (items) => (
  items.find(({ sellers }) => sellers.find(
    ({ commertialOffer }) => commertialOffer.AvailableQuantity > 0,
  ))
);

export default getFirstAvailableSku;