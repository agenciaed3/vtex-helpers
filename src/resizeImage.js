const resizeImage = (src, width, height = 'auto') => {
  console.log(src);
  const newSrc = src.replace(/(?:ids\/[0-9]+)-([0-9]+)-([0-9]+)\//, (match, matchedWidth, matchedHeight) => (match.replace(`-${matchedWidth}-${matchedHeight}`, `-${width}-${height}`)));
  return newSrc.replace(/(ids\/[0-9]+)\//, `$1-${width}-${height}/`);
};
export default resizeImage;