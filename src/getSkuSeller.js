const getSkuSeller = (sku, sellerId) => {
  const { sellers } = sku;
  const seller = sellerId || true;
  const sellerKey = sellerId ? 'sellerId' : 'sellerDefault';
  return sellers.find((item) => item[sellerKey] === seller);
};
export default getSkuSeller;