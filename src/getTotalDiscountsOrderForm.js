import * as R from 'ramda';
const getTotalDiscountsOrderForm = (orderForm) => {
  return R.find(R.propEq('id', 'Discounts'))(orderForm.totalizers);
};

export default getTotalDiscountsOrderForm;
