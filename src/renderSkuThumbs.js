const renderSkuThumbs = (skus) => {
  return skus.map(sku => `<li class="skus__item"><span class="skus__label skuespec_Cor_opcao_01donademim"></span></li>`).join('');
}

export default renderSkuThumbs;