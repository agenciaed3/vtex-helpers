const getPercentage = (oldPrice, newPrice, length = 0) => {
  if (oldPrice < newPrice || oldPrice < 1 || newPrice < 1) {
      return false;
  }
  const percent = (newPrice / oldPrice) * 100 - 100;
  const percentage = Math.abs(+percent.toFixed(length));
  return percentage;
};
export default getPercentage;
