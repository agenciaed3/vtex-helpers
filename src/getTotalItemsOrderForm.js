import * as R from 'ramda';

const getTotalItemsOrderForm = (orderForm) => {
  return R.find(R.propEq('id', 'Items'))(orderForm.totalizers);
};

export default getTotalItemsOrderForm;
