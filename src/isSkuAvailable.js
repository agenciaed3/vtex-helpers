const isSkuAvailable = (sku, sellerId) => {
  const seller = getSkuSeller(sku, sellerId);
  return seller
      && seller.commertialOffer
      && seller.commertialOffer.AvailableQuantity > 0;
};
export default isSkuAvailable;