import * as R from 'ramda';
import EventEmitter from '@ed3digital/event-emitter';

const addToCart = (
  items,
  expectedOrderFormSections = null,
  salesChannel = 1
) => {
  if (!Array.isArray(items)) {
    throw new TypeError(
      `Items precisa ser um Array de Objeto(s) com items para adicionar, exemplo: var items = [{id: 123, quantity: 1, seller: '1'}, {id: 321, quantity: 2, seller: '1'}]`
    );
  }

  if (R.isEmpty(items)) {
    throw new Error(`Items não pode ser um array vazio.`);
  }

  vtexjs.checkout
    .addToCart(items, expectedOrderFormSections, salesChannel)
    .done((orderForm) => {
      EventEmitter.emit('PRODUCT_ADDED_TO_CART', [orderForm, items]);
    })
    .fail((err) => {
      EventEmitter.emit('PRODUCT_NOT_ADDED_TO_CART', [err, items]);
    });
};

export default addToCart;
