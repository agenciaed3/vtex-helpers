import getSkuSeller from './getSkuSeller';
const getSkuInstallments = (sku, sellerId) => {
    const { commertialOffer } = getSkuSeller(sku, sellerId);
    const { Installments } = commertialOffer;
    // Get by min price value
    return Installments.reduce((prev, current) => (prev.Value < current.Value ? prev : current), {});
};
export default getSkuInstallments;