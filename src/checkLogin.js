import axios from 'axios';

const checkLogin = () => {
  const endpoint = '/no-cache/profileSystem/getProfile';
  return new Promise((resolve, reject) => {
    axios
      .get(endpoint)
      .then((res) => resolve(res.data))
      .catch((res) => reject(res));
  });
};

export default checkLogin;
