import * as R from 'ramda';

const openPopupLogin = (noReload, _url) => {
  noReload = R.is(Boolean, noReload) ? noReload : false;
  _url = R.is(String, _url) ? _url : '/';
  _url = noReload ? window.location.href : _url;

  vtexid.start({
    returnUrl: _url,
  });
};

export default openPopupLogin;
