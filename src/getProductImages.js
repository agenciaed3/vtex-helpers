const getProductImages = (sku) => {
  return {
    images: sku.images
  }
}

export default getProductImages;