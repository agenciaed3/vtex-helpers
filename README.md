# ED3 VTEX Helpers

<p>An isomorphic and depency free collection of VTEX APIs methods</p>

## Table of Contents

- [Install](#install)
- [Usage](#usage)
- [API](#api)

## Install

```bash
# Yarn
yarn add @ed3digital/vtex-helpers

# NPM
npm install --save @ed3digital/vtex-helpers
```

## Usage

### Imports

You can import each method individually

```js
import addToCart from '@ed3digital/vtex-helpers/AddToCart';
```

or use ES6 named import (tree shaking recommended)

```js
import { addToCart } from '@ed3digital/vtex-helpers';
```

### Example

```js
var items = [
  {
    id: 1,
    quantity: 1,
    seller: '1',
  },
  {
    id: 2,
    quantity: 2,
    seller: '1',
  },
];

addToCart(items);
```

## Funções

| Name           | Description          | Docs                       |
| :------------- | :------------------- | :------------------------- |
| `addToCart()`  | Add Sku(s) to Cart   | [📝](./docs/addToCart.md)  |
| `checkLogin()` | Check if user logged | [📝](./docs/checkLogin.md) |
